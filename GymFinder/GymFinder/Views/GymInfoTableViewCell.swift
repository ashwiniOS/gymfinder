//
//  GymInfoTableViewCell.swift
//  GymFinder
//
//  Created by Ashwin Nooli on 10/12/18.
//  Copyright © 2018 Ashwin Nooli. All rights reserved.
//

import UIKit

class GymInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var gymName: UILabel!
    @IBOutlet weak var gymAddress: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
