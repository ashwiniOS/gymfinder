//
//  GymListViewController.swift
//  GymFinder
//
//  Created by Ashwin Nooli on 10/12/18.
//  Copyright © 2018 Ashwin Nooli. All rights reserved.
//

import UIKit

class GymListViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var gymListViewModel: GymListViewModel!
    
    var locationManager = GymLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
//        locationManager.startTracking()
        gymListViewModel.fetchGyms(lat: -95390850, long: 51974566)
        {
            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()
            })
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return gymListViewModel.numberOfItemsInSection(section: section)
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let gymCell = tableView.dequeueReusableCell(withIdentifier: "gymInfo", for: indexPath as IndexPath)   as! GymInfoTableViewCell
            
            self.gymListViewModel.configureCell(cell: gymCell, forRowAtIndexPath: indexPath)
            
            return gymCell;
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 100
        }

}
