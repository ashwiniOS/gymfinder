//
//  GymModel.swift
//  GymFinder
//
//  Created by Ashwin Nooli on 10/12/18.
//  Copyright © 2018 Ashwin Nooli. All rights reserved.
//

import UIKit

class GymModel
{
    /// Name of the Gym
    let name : String
    /// Address of the Gym
    let address : String  
    
    init(name:String, address:String ){
        self.name = name
        self.address = address
    }
}
