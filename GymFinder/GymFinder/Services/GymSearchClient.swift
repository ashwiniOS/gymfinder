//
//  GymSearchClient.swift
//  GymFinder
//
//  Created by Ashwin Nooli on 10/12/18.
//  Copyright © 2018 Ashwin Nooli. All rights reserved.
//

import UIKit
import SwiftyJSON

/*! @brief GymSearchClient is a network manager client to invoke webservice calls to fetch gyms from api using client location. This gets called by th view model */
class GymSearchClient: NSObject {
    
    /**
     This function is used to request data from server.
     - Parameter location: The location item is provided by the view model
     - Returns: either success or failer completion block.
     */

    func fetchGymsFromAPI(lat:Double, long:Double , completion:@escaping ([GymModel]?) -> ()) {
        

        let path = "https://private-anon-5b0867f718-fitlgdemo.apiary-mock.com/api/v1/gyms"
        let feedurl = URL(string: path)
        let parameterDictionary = ["latitude" : lat, "longitude" : long]

        var request = URLRequest(url: feedurl!)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        

        let task = session.dataTask(with: request) { (data, response, error) in

            guard error == nil else {
                print((error?.localizedDescription)!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print ("Did not receive data")
                return
            }

            print ("response data \(String(describing: response))")
            // parse the result as JSON, since that's what the API provides
            var tempList:[GymModel] = []
            if let json = try? JSON(data: responseData) {
                for item in json.arrayValue {
                    print(item["name"].stringValue)
                    let gym:GymModel = GymModel(name: item["name"].stringValue, address: item["address"].stringValue)
                    tempList.append(gym)
                     completion(tempList as [GymModel])
                }
            } else{
                print ("error trying to convert data to JSON.")
                return
            }

        }
        task.resume()
    }
    
}
