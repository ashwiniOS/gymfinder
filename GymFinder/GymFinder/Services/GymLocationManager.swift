//
//  GymLocationManager.swift
//  GymFinder
//
//  Created by Ashwin Nooli on 10/12/18.
//  Copyright © 2018 Ashwin Nooli. All rights reserved.
//

import CoreLocation
import Foundation

class GymLocationManager: NSObject, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    
    func startTracking() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager:CLLocationManager, didUpdateLocations locations:[AnyObject]) {
        if  let latValue = locationManager.location?.coordinate.latitude {
             debugPrint(latValue)
        }
        if let lonValue = locationManager.location?.coordinate.longitude {
              debugPrint(lonValue)
        }
    }
}
