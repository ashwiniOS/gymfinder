//
//  GymListViewModel.swift
//  GymFinder
//
//  Created by Ashwin Nooli on 10/12/18.
//  Copyright © 2018 Ashwin Nooli. All rights reserved.
//

import UIKit

class GymListViewModel: NSObject {

    var gymSearchClient = GymSearchClient()
    /// An array to store the gym models.
    var gyms: [GymModel]?
    var tableView: UITableView?
    
    
    override init() {
        /// An intial value for tracks array
        self.gyms = []
    }
    
    // MARK: - Server Requests
    
    func fetchGyms(lat:Double, long:Double,_ completion: @escaping () -> ())
    {
        gymSearchClient.fetchGymsFromAPI(lat: -95390850, long: 51974566){
            
            gyms in
            
            self.gyms = gyms
            completion()
        }
        
    }
    
    func numberOfItemsInSection(section :Int) -> Int {
        
        return gyms?.count ?? 0
    }

    /**
     Configures cell with gym info
     */
    func configureCell(cell: GymInfoTableViewCell, forRowAtIndexPath indexPath: IndexPath){
        guard let currentGym = self.gyms?[indexPath.row] else {
            //do nothing
            return
        }
        cell.gymName.text = currentGym.name
        cell.gymAddress.text = currentGym.address
    }
}
